/*
 * This file is part of UBIFS.
 *
 * Copyright (C) 2006, 2007 Nokia Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Authors: Artem Bityutskiy
 *          Adrian Hunter
 */

/*
 * This file implements most of the debugging stuff which is compiled in only
 * when it is enabled. But some debugging check functions are implemented in
 * corresponging subsystem, just because they are closely related and utilize
 * various local funtions of the subsystems.
 */

#define UBIFS_DBG_PRESERVE_KMALLOC
#define UBIFS_DBG_PRESERVE_UBI

#include "ubifs.h"

#ifdef CONFIG_UBIFS_FS_DEBUG

DEFINE_SPINLOCK(dbg_lock);

static char dbg_get_key_dump_dump_buf[100];

static size_t km_alloc_cnt = 0;
static size_t vm_alloc_cnt = 0;

static const char *get_key_fmt(int fmt)
{
	switch (fmt) {
	case UBIFS_SIMPLE_KEY_FMT:
		return "simple";
	default:
		return "unknown/invalid format";
	}
}

static const char *get_key_hash(int hash)
{
	switch (hash) {
	case UBIFS_KEY_HASH_R5:
		return "R5";
	case UBIFS_KEY_HASH_TEST:
		return "test";
	default:
		return "unknown/invalid name hash";
	}
}

static const char *get_key_type(int type)
{
	switch (type) {
	case UBIFS_INO_KEY:
		return "inode";
	case UBIFS_DENT_KEY:
		return "direntry";
	case UBIFS_DATA_KEY:
		return "data";
	case UBIFS_TRUN_KEY:
		return "truncate";
	default:
		return "unknown/invalid key";
	}
}

const char *dbg_get_key_dump(const struct ubifs_info *c,
			     const union ubifs_key *key)
{
	char *p = &dbg_get_key_dump_dump_buf[0];
	int type = key_type(c, key);

	if (c->key_fmt == UBIFS_SIMPLE_KEY_FMT) {
		switch (type) {
		case UBIFS_INO_KEY:
			sprintf(p, "(%lu, %s)", key_ino(c, key),
			       get_key_type(type));
			break;
		case UBIFS_DENT_KEY:
			sprintf(p, "(%lu, %s, %#08x)", key_ino(c, key),
				get_key_type(type), key_hash(c, key));
			break;
		case UBIFS_DATA_KEY:
			sprintf(p, "(%lu, %s, %u)", key_ino(c, key),
				get_key_type(type), key_block(c, key));
			break;
		case UBIFS_TRUN_KEY:
			sprintf(p, "(%lu, %s)",
				key_ino(c, key), get_key_type(type));
			break;
		default:
			sprintf(p, "(bad key type: %#08x, %#08x)",
				key->u32[0], key->u32[1]);
		}
	} else
		sprintf(p, "bad key format %d", c->key_fmt);

	return p;
}

const char *dbg_ntype(int type)
{
	switch (type) {
	case UBIFS_PAD_NODE:
		return "padding node";
	case UBIFS_SB_NODE:
		return "superblock node";
	case UBIFS_MST_NODE:
		return "master node";
	case UBIFS_REF_NODE:
		return "reference node";
	case UBIFS_INO_NODE:
		return "inode node";
	case UBIFS_DENT_NODE:
		return "direntry node";
	case UBIFS_DATA_NODE:
		return "data node";
	case UBIFS_TRUN_NODE:
		return "truncate node";
	case UBIFS_IDX_NODE:
		return "indexing node";
	case UBIFS_CS_NODE:
		return "commit start node";
	case UBIFS_ORPH_NODE:
		return "orphan node";
	default:
		return "unknown node";
	}
}

const char *dbg_gtype(int type)
{
	switch (type) {
	case UBIFS_NO_NODE_GROUP:
		return "no node group";
	case UBIFS_IN_NODE_GROUP:
		return "in node group";
	case UBIFS_LAST_OF_NODE_GROUP:
		return "last of node group";
	default:
		return "unknown";
	}
}

const char *dbg_cstate(int cmt_state)
{
	switch (cmt_state) {
	case COMMIT_RESTING:
		return "commit resting";
	case COMMIT_BACKGROUND:
		return "background commit requested";
	case COMMIT_REQUIRED:
		return "commit required";
	case COMMIT_RUNNING_BACKGROUND:
		return "BACKGROUND commit running";
	case COMMIT_RUNNING_REQUIRED:
		return "commit running and required";
	case COMMIT_BROKEN:
		return "broken commit";
	default:
		return "unknown commit state";
	}
}

static void dump_ch(const struct ubifs_ch *ch)
{
	printk(KERN_DEBUG "\tmagic          %#x\n", le32_to_cpu(ch->magic));
	printk(KERN_DEBUG "\tcrc            %#x\n", le32_to_cpu(ch->crc));
	printk(KERN_DEBUG "\tnode_type      %d (%s)\n", ch->node_type,
	       dbg_ntype(ch->node_type));
	printk(KERN_DEBUG "\tgroup_type     %d (%s)\n", ch->group_type,
	       dbg_gtype(ch->group_type));
	printk(KERN_DEBUG "\tsqnum          %llu\n", le64_to_cpu(ch->sqnum));
	printk(KERN_DEBUG "\tlen            %u\n", le32_to_cpu(ch->len));
}

void dbg_dump_node(const struct ubifs_info *c, const void *node)
{
	int i, n;
	union ubifs_key key;
	const struct ubifs_ch *ch = node;

	if (dbg_failure_mode)
		return;

	/* If the magic is incorrect, just hexdump the first bytes */
	if (le32_to_cpu(ch->magic) != UBIFS_NODE_MAGIC) {
		printk(KERN_DEBUG "Not a node, first %zu bytes:", UBIFS_CH_SZ);
		print_hex_dump(KERN_DEBUG, "", DUMP_PREFIX_OFFSET, 32, 1,
			       (void *)node, UBIFS_CH_SZ, 1);
		return;
	}

	spin_lock(&dbg_lock);
	dump_ch(node);

	switch (ch->node_type) {
	case UBIFS_PAD_NODE:
	{
		const struct ubifs_pad_node *pad = node;

		printk(KERN_DEBUG "\tpad_len        %u\n",
		       le32_to_cpu(pad->pad_len));
		break;
	}
	case UBIFS_SB_NODE:
	{
		const struct ubifs_sb_node *sup = node;
		unsigned int sup_flags = le32_to_cpu(sup->flags);

		printk(KERN_DEBUG "\tkey_hash       %d (%s)\n",
		       (int)sup->key_hash, get_key_hash(sup->key_hash));
		printk(KERN_DEBUG "\tkey_fmt        %d (%s)\n",
		       (int)sup->key_fmt, get_key_fmt(sup->key_fmt));
		printk(KERN_DEBUG "\tflags          %#x\n", sup_flags);
		printk(KERN_DEBUG "\t\tfast_unmount   %u\n",
		       !!(sup_flags & UBIFS_FLG_FASTUNMNT));
		printk(KERN_DEBUG "\t\tbig_lpt        %u\n",
		       !!(sup_flags & UBIFS_FLG_BIGLPT));
		printk(KERN_DEBUG "\tmin_io_size    %u\n",
		       le32_to_cpu(sup->min_io_size));
		printk(KERN_DEBUG "\tleb_size       %u\n",
		       le32_to_cpu(sup->leb_size));
		printk(KERN_DEBUG "\tleb_cnt        %u\n",
		       le32_to_cpu(sup->leb_cnt));
		printk(KERN_DEBUG "\tmax_leb_cnt    %u\n",
		       le32_to_cpu(sup->max_leb_cnt));
		printk(KERN_DEBUG "\tmax_bud_bytes  %llu\n",
		       le64_to_cpu(sup->max_bud_bytes));
		printk(KERN_DEBUG "\tlog_lebs       %u\n",
		       le32_to_cpu(sup->log_lebs));
		printk(KERN_DEBUG "\tlpt_lebs       %u\n",
		       le32_to_cpu(sup->lpt_lebs));
		printk(KERN_DEBUG "\torph_lebs      %u\n",
		       le32_to_cpu(sup->orph_lebs));
		printk(KERN_DEBUG "\tjhead_cnt      %u\n",
		       le32_to_cpu(sup->jhead_cnt));
		printk(KERN_DEBUG "\tfanout         %u\n",
		       le32_to_cpu(sup->fanout));
		printk(KERN_DEBUG "\tlsave_cnt      %u\n",
		       le32_to_cpu(sup->lsave_cnt));
		printk(KERN_DEBUG "\tdefault_compr  %u\n",
		       (int)le16_to_cpu(sup->default_compr));
		printk(KERN_DEBUG "\trp_size        %llu\n",
		       le64_to_cpu(sup->rp_size));
		printk(KERN_DEBUG "\trp_uid         %u\n",
		       le32_to_cpu(sup->rp_uid));
		printk(KERN_DEBUG "\trp_gid         %u\n",
		       le32_to_cpu(sup->rp_gid));
		break;
	}
	case UBIFS_MST_NODE:
	{
		const struct ubifs_mst_node *mst = node;

		printk(KERN_DEBUG "\thighest_inum   %llu\n",
		       le64_to_cpu(mst->highest_inum));
		printk(KERN_DEBUG "\tcommit number  %llu\n",
		       le64_to_cpu(mst->cmt_no));
		printk(KERN_DEBUG "\tflags          %#x\n",
		       le32_to_cpu(mst->flags));
		printk(KERN_DEBUG "\tlog_lnum       %u\n",
		       le32_to_cpu(mst->log_lnum));
		printk(KERN_DEBUG "\troot_lnum      %u\n",
		       le32_to_cpu(mst->root_lnum));
		printk(KERN_DEBUG "\troot_offs      %u\n",
		       le32_to_cpu(mst->root_offs));
		printk(KERN_DEBUG "\troot_len       %u\n",
		       le32_to_cpu(mst->root_len));
		printk(KERN_DEBUG "\tgc_lnum        %u\n",
		       le32_to_cpu(mst->gc_lnum));
		printk(KERN_DEBUG "\tihead_lnum     %u\n",
		       le32_to_cpu(mst->ihead_lnum));
		printk(KERN_DEBUG "\tihead_offs     %u\n",
		       le32_to_cpu(mst->ihead_offs));
		printk(KERN_DEBUG "\tindex_size     %u\n",
		       le32_to_cpu(mst->index_size));
		printk(KERN_DEBUG "\tlpt_lnum       %u\n",
		       le32_to_cpu(mst->lpt_lnum));
		printk(KERN_DEBUG "\tlpt_offs       %u\n",
		       le32_to_cpu(mst->lpt_offs));
		printk(KERN_DEBUG "\tnhead_lnum     %u\n",
		       le32_to_cpu(mst->nhead_lnum));
		printk(KERN_DEBUG "\tnhead_offs     %u\n",
		       le32_to_cpu(mst->nhead_offs));
		printk(KERN_DEBUG "\tltab_lnum      %u\n",
		       le32_to_cpu(mst->ltab_lnum));
		printk(KERN_DEBUG "\tltab_offs      %u\n",
		       le32_to_cpu(mst->ltab_offs));
		printk(KERN_DEBUG "\tlsave_lnum     %u\n",
		       le32_to_cpu(mst->lsave_lnum));
		printk(KERN_DEBUG "\tlsave_offs     %u\n",
		       le32_to_cpu(mst->lsave_offs));
		printk(KERN_DEBUG "\tlscan_lnum     %u\n",
		       le32_to_cpu(mst->lscan_lnum));
		printk(KERN_DEBUG "\tleb_cnt        %u\n",
		       le32_to_cpu(mst->leb_cnt));
		printk(KERN_DEBUG "\tempty_lebs     %u\n",
		       le32_to_cpu(mst->empty_lebs));
		printk(KERN_DEBUG "\tidx_lebs       %u\n",
		       le32_to_cpu(mst->idx_lebs));
		printk(KERN_DEBUG "\ttotal_free     %llu\n",
		       le64_to_cpu(mst->total_free));
		printk(KERN_DEBUG "\ttotal_dirty    %llu\n",
		       le64_to_cpu(mst->total_dirty));
		printk(KERN_DEBUG "\ttotal_used     %llu\n",
		       le64_to_cpu(mst->total_used));
		printk(KERN_DEBUG "\ttotal_dead     %llu\n",
		       le64_to_cpu(mst->total_dead));
		printk(KERN_DEBUG "\ttotal_dark     %llu\n",
		       le64_to_cpu(mst->total_dark));
		break;
	}
	case UBIFS_REF_NODE:
	{
		const struct ubifs_ref_node *ref = node;

		printk(KERN_DEBUG "\tlnum           %u\n",
		       le32_to_cpu(ref->lnum));
		printk(KERN_DEBUG "\toffs           %u\n",
		       le32_to_cpu(ref->offs));
		printk(KERN_DEBUG "\tjhead          %u\n",
		       le32_to_cpu(ref->jhead));
		break;
	}
	case UBIFS_INO_NODE:
	{
		const struct ubifs_ino_node *ino = node;

		key_read(c, &ino->key, &key);
		printk(KERN_DEBUG "\tkey            %s\n",
		       dbg_get_key_dump(c, &key));
		printk(KERN_DEBUG "\tsize           %llu\n",
		       le64_to_cpu(ino->size));
		printk(KERN_DEBUG "\tnlink          %u\n",
		       le32_to_cpu(ino->nlink));
		printk(KERN_DEBUG "\tatime          %u\n",
		       le32_to_cpu(ino->atime));
		printk(KERN_DEBUG "\tctime          %u\n",
		       le32_to_cpu(ino->ctime));
		printk(KERN_DEBUG "\tmtime          %u\n",
		       le32_to_cpu(ino->mtime));
		printk(KERN_DEBUG "\tuid            %u\n",
		       le32_to_cpu(ino->uid));
		printk(KERN_DEBUG "\tgid            %u\n",
		       le32_to_cpu(ino->gid));
		printk(KERN_DEBUG "\tmode           %u\n",
		       le32_to_cpu(ino->mode));
		printk(KERN_DEBUG "\tflags          %#x\n",
		       le32_to_cpu(ino->flags));
		printk(KERN_DEBUG "\tcompr_type     %#x\n",
		       (int)le16_to_cpu(ino->compr_type));
		printk(KERN_DEBUG "\tdata len       %u\n",
		       le32_to_cpu(ino->data_len));
		break;
	}
	case UBIFS_DENT_NODE:
	{
		const struct ubifs_dent_node *dent = node;
		int nlen = le16_to_cpu(dent->nlen);

		key_read(c, &dent->key, &key);
		printk(KERN_DEBUG "\tkey            %s\n",
		       dbg_get_key_dump(c, &key));
		printk(KERN_DEBUG "\tinum           %llu\n",
		       le64_to_cpu(dent->inum));
		printk(KERN_DEBUG "\ttype           %d\n", (int)dent->type);
		printk(KERN_DEBUG "\tnlen           %d\n", nlen);
		printk(KERN_DEBUG "\tname           ");

		if (nlen > UBIFS_MAX_NLEN) {
			nlen = UBIFS_MAX_NLEN;
			printk(KERN_DEBUG "\tWarning! Node is corrupted\n");
		}

		for (i = 0; i < nlen && dent->name[i]; i++)
			printk("%c", dent->name[i]);
		printk("\n");

		break;
	}
	case UBIFS_DATA_NODE:
	{
		const struct ubifs_data_node *dn = node;
		int dlen = le32_to_cpu(ch->len) - UBIFS_DATA_NODE_SZ;

		key_read(c, &dn->key, &key);
		printk(KERN_DEBUG "\tkey            %s\n",
		       dbg_get_key_dump(c, &key));
		printk(KERN_DEBUG "\tsize           %u\n",
		       le32_to_cpu(dn->size));
		printk(KERN_DEBUG "\tcompr_typ      %d\n",
		       (int)le16_to_cpu(dn->compr_type));
		printk(KERN_DEBUG "\tdata size      %d\n",
		       dlen);
		printk(KERN_DEBUG "\tdata:\n");
		print_hex_dump(KERN_DEBUG, "\t", DUMP_PREFIX_OFFSET, 32, 1,
			       (void *)&dn->data, dlen, 0);
		break;
	}
	case UBIFS_TRUN_NODE:
	{
		const struct ubifs_trun_node *trun = node;

		key_read(c, &trun->key, &key);
		printk(KERN_DEBUG "\tkey            %s\n",
		       dbg_get_key_dump(c, &key));
		printk(KERN_DEBUG "\told_size       %llu\n",
		       le64_to_cpu(trun->old_size));
		printk(KERN_DEBUG "\tnew_size       %llu\n",
		       le64_to_cpu(trun->new_size));
		break;
	}
	case UBIFS_IDX_NODE:
	{
		const struct ubifs_idx_node *idx = node;

		n = le16_to_cpu(idx->child_cnt);
		printk(KERN_DEBUG "\tchild_cnt      %d\n", n);
		printk(KERN_DEBUG "\tlevel          %d\n",
		       (int)le16_to_cpu(idx->level));
		printk(KERN_DEBUG "Branches:\n");

		for (i = 0; i < n && i < c->fanout - 1; i++) {
			const struct ubifs_branch *br;

			br = ubifs_idx_branch(c, idx, i);
			key_read(c, &br->key, &key);
			printk(KERN_DEBUG "\t  %04d: key %s",
			       i, dbg_get_key_dump(c, &key));
			printk(KERN_DEBUG "\t        lnum %6d, offs %6d, "
			       "len %6d\n", le32_to_cpu(br->lnum),
			       le32_to_cpu(br->offs), le32_to_cpu(br->len));
		}
		break;
	}
	case UBIFS_CS_NODE:
		break;
	case UBIFS_ORPH_NODE:
	{
		const struct ubifs_orph_node *orph = node;

		printk(KERN_DEBUG "\tcommit number  %llu\n",
		       le64_to_cpu(orph->cmt_no) & LLONG_MAX);
		printk(KERN_DEBUG "\tlast node flag %llu\n",
		       le64_to_cpu(orph->cmt_no) >> 63);
		n = (le32_to_cpu(ch->len) - UBIFS_ORPH_NODE_SZ) >> 3;
		printk(KERN_DEBUG "\t%d orphan inode numbers:\n", n);
		for (i = 0; i < n; i++)
			printk(KERN_DEBUG "\t  ino %llu\n",
			       le64_to_cpu(orph->inos[i]));
		break;
	}
	default:
		printk(KERN_DEBUG "node type %d was not recognized\n",
		       (int)ch->node_type);
	}
	spin_unlock(&dbg_lock);
}

void dbg_dump_budget_req(const struct ubifs_budget_req *req)
{
	spin_lock(&dbg_lock);
	printk(KERN_DEBUG "Budgeting request: new_ino %d, dirtied_ino %d\n",
	       req->new_ino, req->dirtied_ino);
	printk(KERN_DEBUG "\tnew_ino_d  %d, dirtied_ino_d %d\n",
	       req->new_ino_d, req->dirtied_ino_d);
	printk(KERN_DEBUG "\tnew_page    %d, dirtied_page %d\n",
	       req->new_page, req->dirtied_page);
	printk(KERN_DEBUG "\tnew_dent    %d, rm_dent      %d\n",
	       req->new_dent, req->rm_dent);
	printk(KERN_DEBUG "\tlocked_pg   %d idx_growth    %d\n",
	       req->locked_pg, req->idx_growth);
	printk(KERN_DEBUG "\tdata_growth %d dd_growth     %d\n",
	       req->locked_pg, req->idx_growth);
	spin_unlock(&dbg_lock);
}

void dbg_dump_lstats(const struct ubifs_lp_stats *lst)
{
	spin_lock(&dbg_lock);
	printk(KERN_DEBUG "Lprops statistics: empty_lebs %d, idx_lebs  %d\n",
	       lst->empty_lebs, lst->idx_lebs);
	printk(KERN_DEBUG "\ttaken_empty_lebs %d, total_free %lld, "
	       "total_dirty %lld\n", lst->taken_empty_lebs, lst->total_free,
	       lst->total_dirty);
	printk(KERN_DEBUG "\ttotal_used %lld, total_dark %lld, "
	       "total_dead %lld\n", lst->total_used, lst->total_dark,
	       lst->total_dead);
	spin_unlock(&dbg_lock);
}

void dbg_dump_budg(struct ubifs_info *c)
{
	int i;
	struct rb_node *rb;
	struct ubifs_bud *bud;
	struct ubifs_gced_idx_leb *idx_gc;

	spin_lock(&dbg_lock);
	printk(KERN_DEBUG "Budgeting info: budg_data_growth %lld, "
	       "budg_dd_growth %lld, budg_idx_growth %lld\n",
	       c->budg_data_growth, c->budg_dd_growth, c->budg_idx_growth);
	printk(KERN_DEBUG "\tdata budget sum %lld, total budget sum %lld, "
	       "freeable_cnt %d\n", c->budg_data_growth + c->budg_dd_growth,
	       c->budg_data_growth + c->budg_dd_growth + c->budg_idx_growth,
	       c->freeable_cnt);
	printk(KERN_DEBUG "\tmin_idx_lebs %d, old_idx_sz %lld, "
	       "calc_idx_sz %lld, idx_gc_cnt %d\n", c->min_idx_lebs,
	       c->old_idx_sz, c->calc_idx_sz, c->idx_gc_cnt);
	printk(KERN_DEBUG "\tdirty_pg_cnt %ld, dirty_ino_cnt %ld, "
	       "dirty_zn_cnt %ld, clean_zn_cnt %ld\n",
	       atomic_long_read(&c->dirty_pg_cnt),
	       atomic_long_read(&c->dirty_ino_cnt),
	       atomic_long_read(&c->dirty_zn_cnt),
	       atomic_long_read(&c->clean_zn_cnt));
	printk(KERN_DEBUG "\tdark_wm %d, dead_wm %d, max_idx_node_sz %d\n",
	       c->dark_wm, c->dead_wm, c->max_idx_node_sz);
	printk(KERN_DEBUG "\tgc_lnum %d, ihead_lnum %d\n",
	       c->gc_lnum, c->ihead_lnum);
	for (i = 0; i < c->jhead_cnt; i++)
		printk(KERN_DEBUG "\tjhead %d\t LEB %d\n",
		       c->jheads[i].wbuf.jhead, c->jheads[i].wbuf.lnum);
	for (rb = rb_first(&c->buds); rb; rb = rb_next(rb)) {
		bud = rb_entry(rb, struct ubifs_bud, rb);
		printk(KERN_DEBUG "\tbud LEB %d\n", bud->lnum);
	}
	list_for_each_entry(bud, &c->old_buds, list)
		printk(KERN_DEBUG "\told bud LEB %d\n", bud->lnum);
	list_for_each_entry(idx_gc, &c->idx_gc, list)
		printk(KERN_DEBUG "\tGC'ed idx LEB %d unmap %d\n",
		       idx_gc->lnum, idx_gc->unmap);
	printk(KERN_DEBUG "\tcommit state %d\n", c->cmt_state);
	spin_unlock(&dbg_lock);
}

void dbg_dump_lprop(const struct ubifs_info *c, const struct ubifs_lprops *lp)
{
	printk(KERN_DEBUG "LEB %d lprops: free %d, dirty %d (used %d), "
	       "flags %#x\n", lp->lnum, lp->free, lp->dirty,
	       c->leb_size - lp->free - lp->dirty, lp->flags);
}

void dbg_dump_lprops(struct ubifs_info *c)
{
	int lnum, err;
	struct ubifs_lprops lp;
	struct ubifs_lp_stats lst;

	printk(KERN_DEBUG "Dumping LEB properties\n");
	ubifs_get_lp_stats(c, &lst);
	dbg_dump_lstats(&lst);

	for (lnum = c->main_first; lnum < c->leb_cnt; lnum++) {
		err = ubifs_read_one_lp(c, lnum, &lp);
		if (err)
			ubifs_err("cannot read lprops for LEB %d", lnum);

		dbg_dump_lprop(c, &lp);
	}
}

void dbg_dump_leb(const struct ubifs_info *c, int lnum)
{
	struct ubifs_scan_leb *sleb;
	struct ubifs_scan_node *snod;

	if (dbg_failure_mode)
		return;

	printk(KERN_DEBUG "Dumping LEB %d\n", lnum);

	sleb = ubifs_scan(c, lnum, 0, c->dbg_buf);
	if (IS_ERR(sleb)) {
		ubifs_err("scan error %d", (int)PTR_ERR(sleb));
		return;
	}

	printk(KERN_DEBUG "LEB %d has %d nodes ending at %d\n", lnum,
	       sleb->nodes_cnt, sleb->endpt);

	list_for_each_entry(snod, &sleb->nodes, list) {
		cond_resched();
		printk(KERN_DEBUG "Dumping node at LEB %d:%d len %d\n", lnum,
		       snod->offs, snod->len);
		dbg_dump_node(c, snod->node);
	}

	ubifs_scan_destroy(sleb);
	return;
}

void dbg_dump_znode(const struct ubifs_info *c, const struct ubifs_znode *znode)
{
	int n;

	spin_lock(&dbg_lock);
	printk(KERN_DEBUG "znode %p, parent %p iip %d level %d child_cnt %d "
	       "flags %lx\n", znode, znode->parent, znode->iip, znode->level,
	       znode->child_cnt, znode->flags);

	if (znode->child_cnt <= 0 || znode->child_cnt > c->fanout) {
		spin_unlock(&dbg_lock);
		return;
	}

	printk(KERN_DEBUG "zbranches:\n");
	for (n = 0; n < znode->child_cnt; n++) {
		const struct ubifs_zbranch *zbr = &znode->zbranch[n];

		cond_resched();
		if (znode->level > 0)
			printk(KERN_DEBUG "\t%d: znode %p lnum %d offs %d "
					  "len %d key %s\n", n, zbr->znode,
					  zbr->lnum, zbr->offs, zbr->len,
					  dbg_get_key_dump(c, &zbr->key));
		else
			printk(KERN_DEBUG "\t%d: LNC %p lnum %d offs %d "
					  "len %d key %s\n", n, zbr->znode,
					  zbr->lnum, zbr->offs, zbr->len,
					  dbg_get_key_dump(c, &zbr->key));
	}
	spin_unlock(&dbg_lock);
}

void dbg_dump_heap(struct ubifs_info *c, struct ubifs_lpt_heap *heap, int cat)
{
	int i;

	printk(KERN_DEBUG "dumping heap cat %d (%d elements)\n",
	       cat, heap->cnt);
	for (i = 0; i < heap->cnt; i++) {
		struct ubifs_lprops *lprops = heap->arr[i];

		printk(KERN_DEBUG "\t%d. LEB %d hpos %d free %d dirty %d "
		       "flags %d\n", i, lprops->lnum, lprops->hpos,
		       lprops->free, lprops->dirty, lprops->flags);
	}
}

void *dbg_km_chkr(size_t size, gfp_t flags)
{
	void *addr;

	addr = kmalloc(size, flags);
	if (addr != NULL) {
		spin_lock(&dbg_lock);
		km_alloc_cnt += 1;
		spin_unlock(&dbg_lock);
	}
	return addr;
}

void *dbg_kz_chkr(size_t size, gfp_t flags)
{
	void *addr;

	addr = kzalloc(size, flags);
	if (addr != NULL) {
		spin_lock(&dbg_lock);
		km_alloc_cnt += 1;
		spin_unlock(&dbg_lock);
	}
	return addr;
}

void dbg_kf_chkr(const void *addr)
{
	if (addr != NULL) {
		spin_lock(&dbg_lock);
		km_alloc_cnt -= 1;
		spin_unlock(&dbg_lock);
		kfree(addr);
	}
}

void *dbg_vm_chkr(size_t size)
{
	void *addr;

	addr = vmalloc(size);
	if (addr != NULL) {
		spin_lock(&dbg_lock);
		vm_alloc_cnt += 1;
		spin_unlock(&dbg_lock);
	}
	return addr;
}

void dbg_vf_chkr(void *addr)
{
	if (addr != NULL) {
		spin_lock(&dbg_lock);
		vm_alloc_cnt -= 1;
		spin_unlock(&dbg_lock);
		vfree(addr);
	}
}

void dbg_leak_rpt(void)
{
	spin_lock(&dbg_lock);
	if (km_alloc_cnt || vm_alloc_cnt) {
		ubifs_err("kmalloc: leak count %zd", km_alloc_cnt);
		ubifs_err("vmalloc: leak count %zd", vm_alloc_cnt);
	}
	spin_unlock(&dbg_lock);
}

#endif /* CONFIG_UBIFS_FS_DEBUG */

#ifdef CONFIG_UBIFS_FS_DEBUG_CHK_MEMPRESS

/*
 * The below debugging stuff helps to make fake Linux memory pressure in order
 * to make UBIFS shrinker be invoked. Useful for testing.
 */

/*
 * struct eaten_memory - memory object eaten by UBIFS to cause memory pressure.
 * @list: link in the list of eaten memory objects
 * @pad: just pads to memury page size
 */
struct eaten_memory
{
	struct list_head list;
	uint8_t pad[PAGE_CACHE_SIZE - sizeof(struct list_head)];
};

/* List of eaten memory pages */
LIST_HEAD(eaten_list);
/* Count of allocated 'struct eaten_memory' objects */
static unsigned long eaten_cnt;
/* Protects 'eaten_list' and 'eaten_cnt' */
static DEFINE_SPINLOCK(eaten_lock);

void dbg_eat_memory(void)
{
	struct eaten_memory *em;

	em = kmalloc(sizeof(struct eaten_memory), GFP_NOFS);
	if (!em) {
		ubifs_err("cannot allocate eaten memory structure");
		return;
	}

	spin_lock(&eaten_lock);
	list_add_tail(&em->list, &eaten_list);
	eaten_cnt += 1;
	spin_unlock(&eaten_lock);
}

static int return_eaten_memory(int nr)
{
	int free_all = 0, freed = 0;
	struct eaten_memory *em;

	if (nr == 0)
		return eaten_cnt;

	if (nr == -1)
		free_all = 1;

	while (nr > 0 || free_all) {
		spin_lock(&eaten_lock);
		if (eaten_cnt == 0) {
			spin_unlock(&eaten_lock);
			break;
		}

		em = list_entry(eaten_list.next, struct eaten_memory, list);
		list_del(&em->list);
		eaten_cnt -= 1;
		spin_unlock(&eaten_lock);

		kfree(em);
		nr -= 1;
		freed += 1;
	}

	return freed;
}

static int dbg_shrinker(int nr, gfp_t gfp_mask)
{
	return return_eaten_memory(nr);
}

static struct shrinker dbg_shrinker_info =
{
	.shrink = dbg_shrinker,
	.seeks = DEFAULT_SEEKS,
};

void __init dbg_mempressure_init(void)
{
	register_shrinker(&dbg_shrinker_info);
}

void __exit dbg_mempressure_exit(void)
{
	unregister_shrinker(&dbg_shrinker_info);
	return_eaten_memory(-1);
}

#endif /* CONFIG_UBIFS_FS_DEBUG_CHK_MEMPRESS */

#ifdef CONFIG_UBIFS_FS_DEBUG_TEST_RCVRY

#define chance(n, d) (simple_rand() <= (n) * 32768LL / (d))

struct failure_mode_info
{
	struct list_head list;
	struct ubifs_info *c;
};

static LIST_HEAD(fmi_list);
static DEFINE_SPINLOCK(fmi_lock);

static unsigned int next = 0;

static int simple_rand(void)
{
	if (next == 0)
		next = current->pid;
        next = next * 1103515245 + 12345;
        return (next >> 16) & 32767;
}

void dbg_failure_mode_registration(struct ubifs_info *c)
{
	struct failure_mode_info *fmi;

	fmi = kmalloc(sizeof(struct failure_mode_info), GFP_NOFS);
	if (!fmi) {
		dbg_err("Failed to register failure mode - no memory");
		return;
	}
	fmi->c = c;
	spin_lock(&fmi_lock);
	list_add_tail(&fmi->list, &fmi_list);
	spin_unlock(&fmi_lock);
}

void dbg_failure_mode_deregistration(struct ubifs_info *c)
{
	struct failure_mode_info *fmi, *tmp;

	spin_lock(&fmi_lock);
	list_for_each_entry_safe(fmi, tmp, &fmi_list, list)
		if (fmi->c == c) {
			list_del(&fmi->list);
			kfree(fmi);
		}
	spin_unlock(&fmi_lock);
}

static struct ubifs_info *dbg_find_info(struct ubi_volume_desc *desc)
{
	struct failure_mode_info *fmi;

	spin_lock(&fmi_lock);
	list_for_each_entry(fmi, &fmi_list, list)
		if (fmi->c->ubi == desc) {
			struct ubifs_info *c = fmi->c;

			spin_unlock(&fmi_lock);
			return c;
		}
	spin_unlock(&fmi_lock);
	return NULL;
}

static int in_failure_mode(struct ubi_volume_desc *desc)
{
	struct ubifs_info *c = dbg_find_info(desc);

	if (c)
		return c->failure_mode;
	return 0;
}

static int do_fail(struct ubi_volume_desc *desc, int lnum, int write)
{
	struct ubifs_info *c = dbg_find_info(desc);

	if (!c)
		return 0;
	if (c->failure_mode)
		return 1;
	if (lnum == UBIFS_SB_LNUM)
		return 0;
	else if (lnum == UBIFS_MST_LNUM || lnum == UBIFS_MST_LNUM + 1) {
		if (chance(19, 20))
			return 0;
		dbg_rcvry("failing in master LEB %d", lnum);
	} else if (lnum >= UBIFS_LOG_LNUM && lnum <= c->log_last) {
		if (write && chance(99, 100))
			return 0;
		else if (chance(399, 400))
			return 0;
		dbg_rcvry("failing in log LEB %d", lnum);
	} else if (lnum >= c->lpt_first && lnum <= c->lpt_last) {
		if (write && chance(99, 100))
			return 0;
		else if (chance(399, 400))
			return 0;
		dbg_rcvry("failing in LPT LEB %d", lnum);
	} else if (lnum >= c->orph_first && lnum <= c->orph_last) {
		if (write && chance(9, 10))
			return 0;
		else if (chance(39, 40))
			return 0;
		dbg_rcvry("failing in orphan LEB %d", lnum);
	} else if (lnum == c->ihead_lnum) {
		if (chance(99, 100))
			return 0;
		dbg_rcvry("failing in index head LEB %d", lnum);
	} else if (write && !RB_EMPTY_ROOT(&c->buds) &&
	           ubifs_search_bud(c, lnum) == NULL) {
		if (chance(19, 20))
			return 0;
		dbg_rcvry("failing in non-bud LEB %d", lnum);
	} else if (c->cmt_state == COMMIT_RUNNING_BACKGROUND ||
		   c->cmt_state == COMMIT_RUNNING_REQUIRED) {
		if (chance(999, 1000))
			return 0;
		dbg_rcvry("failing in bud LEB %d commit running", lnum);
	} else {
		if (chance(9999, 10000))
			return 0;
		dbg_rcvry("failing in bud LEB %d commit not running", lnum);
	}
	ubifs_err("*** SETTING FAILURE MODE ON ***");
	c->failure_mode = 1;
	dump_stack();
	return 1;
}

static void cut_data(const void *buf, int len)
{
	int flen, i;
	unsigned char *p = (void *)buf;

	flen = (len * (long long)simple_rand()) >> 15;
	for (i = flen; i < len; i++)
		p[i] = 0xff;
}

int dbg_leb_read(struct ubi_volume_desc *desc, int lnum, char *buf, int offset,
		 int len, int check)
{
	if (in_failure_mode(desc))
		return -EIO;
	return ubi_leb_read(desc, lnum, buf, offset, len, check);
}

int dbg_leb_write(struct ubi_volume_desc *desc, int lnum, const void *buf,
		  int offset, int len, int dtype)
{
	int err;

	if (in_failure_mode(desc))
		return -EIO;
	if (do_fail(desc, lnum, 1))
		cut_data(buf, len);
	err = ubi_leb_write(desc, lnum, buf, offset, len, dtype);
	if (err)
		return err;
	if (in_failure_mode(desc))
		return -EIO;
	return 0;
}

int dbg_leb_change(struct ubi_volume_desc *desc, int lnum, const void *buf,
		   int len, int dtype)
{
	int err;

	if (do_fail(desc, lnum, 1))
		return -EIO;
	err = ubi_leb_change(desc, lnum, buf, len, dtype);
	if (err)
		return err;
	if (do_fail(desc, lnum, 1))
		return -EIO;
	return 0;
}

int dbg_leb_erase(struct ubi_volume_desc *desc, int lnum)
{
	int err;

	if (do_fail(desc, lnum, 0))
		return -EIO;
	err = ubi_leb_erase(desc, lnum);
	if (err)
		return err;
	if (do_fail(desc, lnum, 0))
		return -EIO;
	return 0;
}

int dbg_leb_unmap(struct ubi_volume_desc *desc, int lnum)
{
	int err;

	if (do_fail(desc, lnum, 0))
		return -EIO;
	err = ubi_leb_unmap(desc, lnum);
	if (err)
		return err;
	if (do_fail(desc, lnum, 0))
		return -EIO;
	return 0;
}

int dbg_is_mapped(struct ubi_volume_desc *desc, int lnum)
{
	if (in_failure_mode(desc))
		return -EIO;
	return ubi_is_mapped(desc, lnum);
}

#endif /* CONFIG_UBIFS_FS_DEBUG_TEST_RCVRY */

/*
 * Backward compatibility stuff.
 *
 * TODO: remove as late as possible.
 */
#include <linux/version.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,23))

#define BYTES_PER_LINE 32

/**
 * ubifs_hexdump - dump a buffer.
 * @ptr: the buffer to dump
 * @size: buffer size which must be multiple of 4 bytes
 */
void ubifs_hexdump(const void *ptr, int size)
{
	int i, k = 0, rows, columns;
	const uint8_t *p = ptr;

	rows = size / BYTES_PER_LINE + size % BYTES_PER_LINE;
	for (i = 0; i < rows; i++) {
		int j;

		cond_resched();
		columns = min(size - k, BYTES_PER_LINE) / 4;
		if (columns == 0)
			break;
		printk(KERN_DEBUG "%5d:  ", i * BYTES_PER_LINE);
		for (j = 0; j < columns; j++) {
			int n, N;

			N = size - k > 4 ? 4 : size - k;
			for (n = 0; n < N; n++)
				printk("%02x", p[k++]);
			printk(" ");
		}
		printk("\n");
	}
}

#endif /* LINUX_VERSION_CODE < 2.6.23 */

#ifdef UBIFS_COMPAT_USE_OLD_IGET
struct inode *ubifs_iget(struct super_block *sb, unsigned long inum)
{
	struct inode *inode;

	inode = iget(sb, inum);
	if (!inode) {
		make_bad_inode(inode);
		return ERR_PTR(-EINVAL);
	}

	return inode;
}

void ubifs_read_inode(struct inode *inode)
{
	int err;
	union ubifs_key key;
	struct ubifs_ino_node *ino;
	struct ubifs_info *c = inode->i_sb->s_fs_info;
	struct ubifs_inode *ui = ubifs_inode(inode);

	dbg_gen("inode %lu", inode->i_ino);
	ubifs_assert(inode->i_state & I_LOCK);

	ino = kmalloc(UBIFS_MAX_INO_NODE_SZ, GFP_NOFS);
	if (!ino) {
		err = -ENOMEM;
		goto out;
	}

	ino_key_init(c, &key, inode->i_ino);

	err = ubifs_tnc_lookup(c, &key, ino);
	if (err)
		goto out_ino;

	inode->i_flags |= (S_NOCMTIME | S_NOATIME);
	inode->i_nlink = le32_to_cpu(ino->nlink);
	inode->i_uid   = le32_to_cpu(ino->uid);
	inode->i_gid   = le32_to_cpu(ino->gid);
	inode->i_atime.tv_sec = le32_to_cpu(ino->atime);
	inode->i_mtime.tv_sec = le32_to_cpu(ino->mtime);
	inode->i_ctime.tv_sec = le32_to_cpu(ino->ctime);
	inode->i_atime.tv_nsec = inode->i_mtime.tv_nsec =
					inode->i_ctime.tv_nsec = 0;
	inode->i_mode  = le32_to_cpu(ino->mode);
	inode->i_size  = le64_to_cpu(ino->size);

	ubifs_set_i_bytes(inode);

	ui->data_len = le32_to_cpu(ino->data_len);
	ui->flags = le32_to_cpu(ino->flags);
	ui->compr_type = le16_to_cpu(ino->compr_type);
	ui->creat_sqnum = le64_to_cpu(ino->creat_sqnum);

	if (inode->i_size > c->max_inode_sz) {
		ubifs_err("inode is too large (%lld)",
			  (long long)inode->i_size);
		goto out_invalid;
	}
	if (ui->compr_type < 0 || ui->compr_type >= UBIFS_COMPR_TYPES_CNT) {
		ubifs_err("unknown compression type %d", ui->compr_type);
		goto out_invalid;
	}

	if (!ubifs_compr_present(ui->compr_type)) {
		ubifs_warn("inode %lu uses '%s' compression, but it was not "
			   "compiled in", inode->i_ino,
			   ubifs_compr_name(ui->compr_type));
	}

	switch (inode->i_mode & S_IFMT) {
	case S_IFREG:
		inode->i_mapping->a_ops = &ubifs_file_address_operations;
		inode->i_op = &ubifs_file_inode_operations;
		inode->i_fop = &ubifs_file_operations;
		if (ui->data_len != 0)
			goto out_invalid;
		break;
	case S_IFDIR:
		inode->i_op  = &ubifs_dir_inode_operations;
		inode->i_fop = &ubifs_dir_operations;
		if (ui->data_len != 0)
			goto out_invalid;
		break;
	case S_IFLNK:
		inode->i_op = &ubifs_symlink_inode_operations;
		if (ui->data_len <= 0 || ui->data_len > UBIFS_MAX_INO_DATA) {
			ubifs_err("invalid inode size");
			goto out_invalid;
		}
		ui->data = kmalloc(ui->data_len + 1, GFP_KERNEL);
		if (!ui->data) {
			err = -ENOMEM;
			goto out_ino;
		}
		memcpy(ui->data, ino->data, ui->data_len);
		((char *)ui->data)[ui->data_len] = '\0';
		break;
	case S_IFBLK:
	case S_IFCHR:
	{
		dev_t rdev;
		union ubifs_dev_desc *dev;
		struct ubifs_inode *ui = ubifs_inode(inode);

		ui->data = kmalloc(sizeof(union ubifs_dev_desc), GFP_NOFS);
		if (!ui->data) {
			err = -ENOMEM;
			goto out_ino;
		}

		dev = (union ubifs_dev_desc *)ino->data;
		if (ui->data_len == sizeof(dev->new)) {
			rdev = new_decode_dev(__le32_to_cpu(dev->new));
		} else if (ui->data_len == sizeof(dev->huge)) {
			rdev = huge_decode_dev(__le64_to_cpu(dev->huge));
		} else {
			ubifs_err("invalid inode size");
			goto out_invalid;
		}
		inode->i_op = &ubifs_file_inode_operations;
		init_special_inode(inode, inode->i_mode, rdev);
		break;
	}
	case S_IFSOCK:
	case S_IFIFO:
		inode->i_op = &ubifs_file_inode_operations;
		init_special_inode(inode, inode->i_mode, 0);
		if (ui->data_len != 0)
			goto out_invalid;
		break;
	default:
		goto out_invalid;
	}

	ubifs_set_inode_flags(inode);
	kfree(ino);
	return;

out_invalid:
	ubifs_err("inode %lu validation failed", inode->i_ino);
	dbg_dump_node(c, ino);
	err = -EINVAL;
out_ino:
	kfree(ino);
out:
	ubifs_err("failed to read inode %lu, error %d", inode->i_ino, err);
	make_bad_inode(inode);
	return;
}

#endif /* UBIFS_COMPAT_USE_OLD_IGET */

#ifdef UBIFS_COMPAT_USE_OLD_PREPARE_WRITE

int ubifs_prepare_write(struct file *file, struct page *page, unsigned from,
			unsigned to)
{
	struct inode *inode = page->mapping->host;
	struct ubifs_info *c = inode->i_sb->s_fs_info;
	loff_t pos = ((loff_t)page->index << PAGE_CACHE_SHIFT) + to;
	struct ubifs_budget_req req;
	int err;

	ubifs_assert(PageLocked(page));
	ubifs_assert(mutex_is_locked(&inode->i_mutex));
	ubifs_assert(!(inode->i_sb->s_flags & MS_RDONLY));
	dbg_eat_memory();

	if (c->ro_media)
		return -EINVAL;

	if (!PageUptodate(page)) {
		/*
		 * The page is not loaded from the flash and has to be loaded
		 * unless we are writing all of it.
		 */
		if (from == 0 && to == PAGE_CACHE_SIZE)
			/*
			 * Set the PG_checked flag to make the further code
			 * allocate full budget, because we do not know whether
			 * the page exists on the flash media or not.
			 */
			SetPageChecked(page);
		else {
			err = do_readpage(page);
			if (err)
				return err;
		}

		SetPageUptodate(page);
		ClearPageError(page);
	}

	memset(&req, 0, sizeof(struct ubifs_budget_req));
	if (!PagePrivate(page)) {
		/*
		 * If the PG_Checked flag is set, the page corresponds to a
		 * hole or to a place beyond the inode. In this case we have to
		 * budget for a new page, otherwise for a dirtied page.
		 */
		if (PageChecked(page))
			req.new_page = 1;
		else
			req.dirtied_page = 1;
	} else
		req.locked_pg = 1;

	if (pos > inode->i_size)
		/*
		 * We are writing beyond the file which means we are going to
		 * change inode size and make the inode dirty. And in turn,
		 * this means we have to budget for making the inode dirty.
		 *
		 * Note, if the inode is already dirty,
		 * 'ubifs_budget_operation()' will not allocate any budget, but
		 * will just lock the @budg_mutex of the inode to prevent it
		 * from becoming clean before we have changed its size, which is
		 * going to happen in 'ubifs_write_end()'.
		 */
		err = ubifs_budget_operation(c, inode, &req);
	else
		/*
		 * The inode is not going to be marked as dirty by this write
		 * operation, do do not budget for this.
		 */
		err = ubifs_budget_space(c, &req);

	return err;
}

int ubifs_commit_write(struct file *file, struct page *page, unsigned from,
		       unsigned to)
{
	struct inode *inode = page->mapping->host;
	struct ubifs_inode *ui = ubifs_inode(inode);
	struct ubifs_info *c = inode->i_sb->s_fs_info;
	loff_t pos = ((loff_t)page->index << PAGE_CACHE_SHIFT) + to;

	dbg_gen("ino %lu, pg %lu, offs %lld-%lld (in pg: %u-%u, %u bytes) "
		"flags %#lx", inode->i_ino, page->index, pos - to + from,
		pos, from, to, to - from, page->flags);
	ubifs_assert(PageUptodate(page));
	ubifs_assert(mutex_is_locked(&inode->i_mutex));

	if (!PagePrivate(page)) {
		SetPagePrivate(page);
		atomic_long_inc(&c->dirty_pg_cnt);
		__set_page_dirty_nobuffers(page);
	}

	if (pos > inode->i_size) {
		i_size_write(inode, pos);
		ubifs_set_i_bytes(inode);

		/*
		 * Note, we do not set 'I_DIRTY_PAGES' (which means that the
		 * inode has dirty pages), this has been done in
		 * '__set_page_dirty_nobuffers()'.
		 */
		mark_inode_dirty_sync(inode);

		/*
		 * The inode has been marked dirty, unlock it. This is a bit
		 * hacky because normally we would have to call
		 * 'ubifs_release_op_budget()'. But we know there is nothing to
		 * release because page's budget will be released in
		 * 'ubifs_write_page()' and inode's budget will be released in
		 * 'ubifs_write_inode()', so just unlock the inode here for
		 * optimization.
		 */
		mutex_unlock(&ui->budg_mutex);
	}

	return 0;
}

#include <linux/writeback.h>

#define MAX_SHINK_RETRIES 8
#define MAX_GC_RETRIES    4
#define MAX_CMT_RETRIES   2
#define MAX_NOSPC_RETRIES 1
#define NR_TO_WRITE 16

struct retries_info {
	long long prev_liability;
	unsigned int shrink_cnt;
	unsigned int shrink_retries:5;
	unsigned int try_gc:1;
	unsigned int gc_retries:4;
	unsigned int cmt_retries:3;
	unsigned int nospc_retries:1;
};

static int shrink_liability(struct ubifs_info *c, int nr_to_write,
			    int locked_pg)
{
	struct writeback_control wbc = {
		.sync_mode   = WB_SYNC_NONE,
		.range_end   = LLONG_MAX,
		.nr_to_write = nr_to_write,
		.skip_locked_pages = locked_pg,
	};

	writeback_inodes_sb(c->vfs_sb, &wbc);
	dbg_budg("%ld pages were written back", nr_to_write - wbc.nr_to_write);
	return nr_to_write - wbc.nr_to_write;
}

static int run_gc(struct ubifs_info *c)
{
	int err, lnum;

	/* Make some free space by garbage-collecting dirty space */
	down_read(&c->commit_sem);
	lnum = ubifs_garbage_collect(c, 1);
	up_read(&c->commit_sem);
	if (lnum < 0)
		return lnum;

	/* GC freed one LEB, return it to lprops */
	dbg_budg("GC freed LEB %d", lnum);
	err = ubifs_return_leb(c, lnum);
	if (err)
		return err;

	return 0;
}

int ubifs_make_free_space(struct ubifs_info *c, struct retries_info *ri,
			  int locked_pg)
{
	int err;

	/*
	 * If we have some dirty pages and inodes (liability), try to write
	 * them back unless this was tried too many times without effect
	 * already.
	 */
	if (ri->shrink_retries < MAX_SHINK_RETRIES && !ri->try_gc) {
		long long liability;

		spin_lock(&c->space_lock);
		liability = c->budg_idx_growth + c->budg_data_growth +
			    c->budg_dd_growth;
		spin_unlock(&c->space_lock);

		if (ri->prev_liability >= liability) {
			/* Liability does not shrink, next time try GC then */
			ri->shrink_retries += 1;
			if (ri->gc_retries < MAX_GC_RETRIES)
				ri->try_gc = 1;
			dbg_budg("liability did not shrink: retries %d of %d",
				 ri->shrink_retries, MAX_SHINK_RETRIES);
		}

		dbg_budg("force write-back (count %d)", ri->shrink_cnt);
		shrink_liability(c, NR_TO_WRITE + ri->shrink_cnt, locked_pg);

		ri->prev_liability = liability;
		ri->shrink_cnt += 1;
		return -EAGAIN;
	}

	/*
	 * Try to run garbage collector unless it was already tried too many
	 * times.
	 */
	if (ri->gc_retries < MAX_GC_RETRIES) {
		ri->gc_retries += 1;
		dbg_budg("run GC, retries %d of %d",
			 ri->gc_retries, MAX_GC_RETRIES);

		ri->try_gc = 0;
		err = run_gc(c);
		if (!err)
			return -EAGAIN;

		if (err == -EAGAIN) {
			dbg_budg("GC asked to commit");
			err = ubifs_run_commit(c);
			if (err)
				return err;
			return -EAGAIN;
		}

		if (err != -ENOSPC)
			return err;

		/*
		 * GC could not make any progress. If this is the first time,
		 * then it makes sense to try to commit, because it might make
		 * some dirty space.
		 */
		dbg_budg("GC returned -ENOSPC, retries %d",
			 ri->nospc_retries);
		if (ri->nospc_retries >= MAX_NOSPC_RETRIES)
			return err;
		ri->nospc_retries += 1;
	}

	/* Neither GC nor write-back helped, try to commit */
	if (ri->cmt_retries < MAX_CMT_RETRIES) {
		ri->cmt_retries += 1;
		dbg_budg("run commit, retries %d of %d",
			 ri->cmt_retries, MAX_CMT_RETRIES);
		err = ubifs_run_commit(c);
		if (err)
			return err;
		return -EAGAIN;
	}

	return -ENOSPC;
}

#endif /* UBIFS_COMPAT_USE_OLD_PREPARE_WRITE */
