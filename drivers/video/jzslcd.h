/*
 * linux/drivers/video/jzslcd.h -- Ingenic On-Chip SLCD frame buffer device
 *
 * Copyright (C) 2005-2007, Ingenic Semiconductor Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#ifndef __JZSLCD_H__
#define __JZSLCD_H__

#define UINT16 unsigned short
#define UINT32 unsigned int

#define NR_PALETTE	256
/* Jz LCDFB supported I/O controls. */
#define FBIOSETBACKLIGHT	0x4688
#define FBIODISPON		0x4689
#define FBIODISPOFF		0x468a
#define FBIORESET		0x468b
#define FBIOPRINT_REG		0x468c
#define FBIO_REFRESH_ALWAYS	0x468d
#define FBIO_REFRESH_EVENTS	0x468e
#define FBIO_DO_REFRESH		0x468f

#if defined(CONFIG_JZ_SLCD_G240400RTSW_16BUS)

#define ORI_HORIZONTAL           1
#define ORI_VERTICAL               2

#define DISPLAY_ORI           ORI_VERTICAL



#define PIN_CS_N    (32*1+17) /* Chip select */
#define PIN_RESET_N (32*1+18) /* Reset */
/* Register list from rockbox*/
#define REG_DRIVER_OUTPUT                0x001
#define REG_LCD_DR_WAVE_CTRL             0x002
#define REG_ENTRY_MODE                   0x003
#define REG_OUTL_SHARP_CTRL              0x006
#define REG_DISP_CTRL1                   0x007
#define REG_DISP_CTRL2                   0x008
#define REG_DISP_CTRL3                   0x009
#define REG_LPCTRL                       0x00B
#define REG_EXT_DISP_CTRL1               0x00C
#define REG_EXT_DISP_CTRL2               0x00F
#define REG_PAN_INTF_CTRL1               0x010
#define REG_PAN_INTF_CTRL2               0x011
#define REG_PAN_INTF_CTRL3               0x012
#define REG_PAN_INTF_CTRL4               0x020
#define REG_PAN_INTF_CTRL5               0x021
#define REG_PAN_INTF_CTRL6               0x022
#define REG_FRM_MRKR_CTRL                0x090

#define REG_PWR_CTRL1                    0x100
#define REG_PWR_CTRL2                    0x101
#define REG_PWR_CTRL3                    0x102
#define REG_PWR_CTRL4                    0x103
#define REG_PWR_CTRL5                    0x107
#define REG_PWR_CTRL6                    0x110
#define REG_PWR_CTRL7                    0x112

#define REG_RAM_HADDR_SET                0x200
#define REG_RAM_VADDR_SET                0x201
#define REG_RW_GRAM                      0x202
#define REG_RAM_HADDR_START              0x210
#define REG_RAM_HADDR_END                0x211
#define REG_RAM_VADDR_START              0x212
#define REG_RAM_VADDR_END                0x213
#define REG_RW_NVM                       0x280
#define REG_VCOM_HVOLTAGE1               0x281
#define REG_VCOM_HVOLTAGE2               0x282

#define REG_GAMMA_CTRL1                  0x300
#define REG_GAMMA_CTRL2                  0x301
#define REG_GAMMA_CTRL3                  0x302
#define REG_GAMMA_CTRL4                  0x303
#define REG_GAMMA_CTRL5                  0x304
#define REG_GAMMA_CTRL6                  0x305
#define REG_GAMMA_CTRL7                  0x306
#define REG_GAMMA_CTRL8                  0x307
#define REG_GAMMA_CTRL9                  0x308
#define REG_GAMMA_CTRL10                 0x309
#define REG_GAMMA_CTRL11                 0x30A
#define REG_GAMMA_CTRL12                 0x30B
#define REG_GAMMA_CTRL13                 0x30C
#define REG_GAMMA_CTRL14                 0x30D

#define REG_BIMG_NR_LINE                 0x400
#define REG_BIMG_DISP_CTRL               0x401
#define REG_BIMG_VSCROLL_CTRL            0x404

#define REG_PARTIMG1_POS                 0x500
#define REG_PARTIMG1_RAM_START           0x501
#define REG_PARTIMG1_RAM_END             0x502
#define REG_PARTIMG2_POS                 0x503
#define REG_PARTIMG2_RAM_START           0x504
#define REG_PARTIMG2_RAM_END             0x505

#define REG_SOFT_RESET                   0x600
#define REG_ENDIAN_CTRL                  0x606
#define REG_NVM_ACCESS_CTRL              0x6F0

/* Bits */
#define DRIVER_OUTPUT_SS_BIT             (1 << 8)
#define DRIVER_OUTPUT_SM_BIT             (1 << 10)

#define ENTRY_MODE_TRI                   (1 << 15)
#define ENTRY_MODE_DFM                   (1 << 14)
#define ENTRY_MODE_BGR                   (1 << 12)
#define ENTRY_MODE_HWM                   (1 << 9)
#define ENTRY_MODE_ORG                   (1 << 7)
#define ENTRY_MODE_VID                   (1 << 5)
#define ENTRY_MODE_HID                   (1 << 4)
#define ENTRY_MODE_AM                    (1 << 3)
#define ENTRY_MODE_EPF(n)                (n & 3)

#define OUTL_SHARP_CTRL_EGMODE           (1 << 15)
#define OUTL_SHARP_CTRL_AVST(n)          ((n & 7) << 7)
#define OUTL_SHARP_CTRL_ADST(n)          ((n & 7) << 4)
#define OUTL_SHARP_CTRL_DTHU(n)          ((n & 3) << 2)
#define OUTL_SHARP_CTRL_DTHL(n)          (n & 3)

#define DISP_CTRL1_PTDE(n)               ((n & 4) << 12)
#define DISP_CTRL1_BASEE                 (1 << 8)
#define DISP_CTRL1_VON                   (1 << 6)
#define DISP_CTRL1_GON                   (1 << 5)
#define DISP_CTRL1_DTE                   (1 << 4)
#define DISP_CTRL1_D(n)                  (n & 3)

#define PWR_CTRL1_SAP(n)                 ((n & 3) << 13)
#define PWR_CTRL1_SAPE                   (1 << 12)
#define PWR_CTRL1_BT(n)                  ((n & 7) << 8)
#define PWR_CTRL1_APE                    (1 << 7)
#define PWR_CTRL1_AP(n)                  ((n & 7) << 4)
#define PWR_CTRL1_DSTB                   (1 << 2)
#define PWR_CTRL1_SLP                    (1 << 1)

#define SOFT_RESET(n)                    (n << 0)

#define BACKLIGHT_GPIO  (32*3+31)
#define BACKLIGHT_PWM   7

#define WAIT_ON_SLCD while(REG_SLCD_STATE & SLCD_STATE_BUSY);
#define SLCD_SET_DATA(x) WAIT_ON_SLCD; REG_SLCD_DATA = (x) | SLCD_DATA_RS_DATA;
#define SLCD_SET_COMMAND(x) WAIT_ON_SLCD; REG_SLCD_DATA = (x) | SLCD_DATA_RS_COMMAND;
#define SLCD_SEND_COMMAND(cmd,val) SLCD_SET_COMMAND(cmd); SLCD_SET_DATA(val);

#define __slcd_dma_enable() (REG_SLCD_CTRL |= SLCD_CTRL_DMA_EN)
#define __slcd_set_backlight_level(n)

/* LCD_D0~LCD_D7, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_8bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x001800ff;	\
	REG_GPIO_PXSELC(2) = 0x001800ff;	\
} while (0)

/* LCD_D0~LCD_D7, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_9bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x001801ff;	\
	REG_GPIO_PXSELC(2) = 0x001801ff;	\
} while (0)

/* LCD_D0~LCD_D15, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_16bit()         \
do {                                    \
    REG_GPIO_PXFUNS(2) = 0x001cffff;    \
    REG_GPIO_PXSELC(2) = 0x001cffff;    \
    REG_GPIO_PXPES(2) = 0x001cffff;     \
} while (0)

/* LCD_D0~LCD_D17, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_18bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x001bffff;	\
	REG_GPIO_PXSELC(2) = 0x001bffff;	\
} while (0)

#define	__slcd_special_pin_init() \
do { \
	__gpio_as_output(PIN_CS_N); \
    __gpio_as_output(PIN_RESET_N); \
    __gpio_clear_pin(PIN_CS_N); \
    __gpio_set_pin(PIN_RESET_N);\
    mdelay(5);\
    __gpio_clear_pin(PIN_RESET_N);\
    mdelay(5);\
    __gpio_set_pin(PIN_RESET_N);\
    mdelay(5);\
    __gpio_as_output(BACKLIGHT_GPIO);\
    __gpio_set_pin(BACKLIGHT_GPIO);\
	mdelay(100);			\
} while(0)

#define __slcd_display_pin_init() \
do { \
	__slcd_special_pin_init(); \
} while (0)

#if DISPLAY_ORI == ORI_VERTICAL

#define __slcd_entry_mode()\
do { \
	SLCD_SEND_COMMAND(REG_ENTRY_MODE,\
                      (ENTRY_MODE_BGR | ENTRY_MODE_VID | ENTRY_MODE_HID |\
                       ENTRY_MODE_HWM));\
} while (0)
#else
#define __slcd_entry_mode()\
do { \
SLCD_SEND_COMMAND(REG_ENTRY_MODE,\
                      (ENTRY_MODE_BGR | ENTRY_MODE_VID | ENTRY_MODE_AM |\
                      ENTRY_MODE_HWM));\
} while (0)
#endif

#define __slcd_special_on() 		\
do {	/* RESET# */			\
	mdelay(1);\
    SLCD_SEND_COMMAND(REG_SOFT_RESET, SOFT_RESET(1));\
    mdelay(2);\
    SLCD_SEND_COMMAND(REG_SOFT_RESET, SOFT_RESET(0));\
    mdelay(2);\
    SLCD_SEND_COMMAND(REG_ENDIAN_CTRL, 0);\
    SLCD_SEND_COMMAND(REG_DRIVER_OUTPUT, 0x100);\
    SLCD_SEND_COMMAND(REG_LCD_DR_WAVE_CTRL, 0x100);\
	__slcd_entry_mode();\
    SLCD_SEND_COMMAND(REG_DISP_CTRL2, 0x503);\
    SLCD_SEND_COMMAND(REG_DISP_CTRL3, 1);\
    SLCD_SEND_COMMAND(REG_LPCTRL, 0x10);\
    SLCD_SEND_COMMAND(REG_EXT_DISP_CTRL1, 0);\
    SLCD_SEND_COMMAND(REG_EXT_DISP_CTRL2, 0);\
    SLCD_SEND_COMMAND(REG_DISP_CTRL1, DISP_CTRL1_D(1));\
    SLCD_SEND_COMMAND(REG_PAN_INTF_CTRL1, 0x12);\
    SLCD_SEND_COMMAND(REG_PAN_INTF_CTRL2, 0x202);\
    SLCD_SEND_COMMAND(REG_PAN_INTF_CTRL3, 0x300);\
    SLCD_SEND_COMMAND(REG_PAN_INTF_CTRL4, 0x21e);\
    SLCD_SEND_COMMAND(REG_PAN_INTF_CTRL5, 0x202);\
    SLCD_SEND_COMMAND(REG_PAN_INTF_CTRL6, 0x100);\
    SLCD_SEND_COMMAND(REG_FRM_MRKR_CTRL, 0x8000);\
    SLCD_SEND_COMMAND(REG_PWR_CTRL1,\
                      (PWR_CTRL1_SAPE | PWR_CTRL1_BT(6) | PWR_CTRL1_APE |\
                       PWR_CTRL1_AP(3)));\
    SLCD_SEND_COMMAND(REG_PWR_CTRL2, 0x147);\
    SLCD_SEND_COMMAND(REG_PWR_CTRL3, 0x1bd);\
    SLCD_SEND_COMMAND(REG_PWR_CTRL4, 0x2f00);\
    SLCD_SEND_COMMAND(REG_PWR_CTRL5, 0);\
    SLCD_SEND_COMMAND(REG_PWR_CTRL6, 1);\
    SLCD_SEND_COMMAND(REG_RAM_HADDR_SET, 0);    /* set cursor at x_start */\
    SLCD_SEND_COMMAND(REG_RAM_VADDR_SET, 0);    /* set cursor at y_start */\
    SLCD_SEND_COMMAND(REG_RAM_HADDR_START, 0);  /* y_start */\
    SLCD_SEND_COMMAND(REG_RAM_HADDR_END, 239);  /* y_end */\
    SLCD_SEND_COMMAND(REG_RAM_VADDR_START, 0);  /* x_start */\
    SLCD_SEND_COMMAND(REG_RAM_VADDR_END, 399);  /* x_end */\
    SLCD_SEND_COMMAND(REG_RW_NVM, 0);\
    SLCD_SEND_COMMAND(REG_VCOM_HVOLTAGE1, 6);\
    SLCD_SEND_COMMAND(REG_VCOM_HVOLTAGE2, 0);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL1, 0x101);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL2, 0xb27);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL3, 0x132a);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL4, 0x2a13);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL5, 0x270b);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL6, 0x101);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL7, 0x1205);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL8, 0x512);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL9, 5);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL10, 3);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL11, 0xf04);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL12, 0xf00);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL13, 0xf);\
    SLCD_SEND_COMMAND(REG_GAMMA_CTRL14, 0x40f);\
    SLCD_SEND_COMMAND(0x30e, 0x300);\
    SLCD_SEND_COMMAND(0x30f, 0x500);\
    SLCD_SEND_COMMAND(REG_BIMG_NR_LINE, 0x3100);\
    SLCD_SEND_COMMAND(REG_BIMG_DISP_CTRL, 1);\
    SLCD_SEND_COMMAND(REG_BIMG_VSCROLL_CTRL, 0);\
    SLCD_SEND_COMMAND(REG_PARTIMG1_POS, 0);\
    SLCD_SEND_COMMAND(REG_PARTIMG1_RAM_START, 0);\
    SLCD_SEND_COMMAND(REG_PARTIMG1_RAM_END, 0);\
    SLCD_SEND_COMMAND(REG_PARTIMG2_POS, 0);\
    SLCD_SEND_COMMAND(REG_PARTIMG2_RAM_START, 0);\
    SLCD_SEND_COMMAND(REG_PARTIMG2_RAM_END, 0);\
    SLCD_SEND_COMMAND(REG_ENDIAN_CTRL, 0);\
    SLCD_SEND_COMMAND(REG_NVM_ACCESS_CTRL, 0);\
    SLCD_SEND_COMMAND(0x7f0, 0x5420);\
    SLCD_SEND_COMMAND(0x7f3, 0x288a);\
    SLCD_SEND_COMMAND(0x7f4, 0x22);\
    SLCD_SEND_COMMAND(0x7f5, 1);\
    SLCD_SEND_COMMAND(0x7f0, 0);\
    SLCD_SEND_COMMAND(REG_DISP_CTRL1, (DISP_CTRL1_BASEE | DISP_CTRL1_VON\
                                       | DISP_CTRL1_GON | DISP_CTRL1_DTE |\
                                       DISP_CTRL1_D(3)));\
    mdelay(5);\
    SLCD_SEND_COMMAND(REG_DISP_CTRL1, (DISP_CTRL1_BASEE | DISP_CTRL1_VON\
                                       | DISP_CTRL1_GON | DISP_CTRL1_DTE |\
                                       DISP_CTRL1_D(2)));\
    mdelay(5);\
    SLCD_SEND_COMMAND(REG_DISP_CTRL1, (DISP_CTRL1_BASEE | DISP_CTRL1_VON\
                                       | DISP_CTRL1_GON | DISP_CTRL1_DTE |\
                                       DISP_CTRL1_D(3)));\
    mdelay(5);\
} while (0)

#endif


#if defined(CONFIG_JZ_SLCD_LGDP4551_8BUS) || defined(CONFIG_JZ_SLCD_LGDP4551_18BUS)
#define PIN_CS_N 	(32*2+18)	/* Chip select      :SLCD_WR: GPC18 */ 
#define PIN_RESET_N 	(32*2+21)	/* LCD reset        :SLCD_RST: GPC21*/ 
#define PIN_RS_N 	(32*2+19)

#define	__slcd_special_pin_init() \
do { \
	__gpio_as_output(PIN_CS_N); 	\
	__gpio_as_output(PIN_RESET_N); 	\
	__gpio_clear_pin(PIN_CS_N); /* Clear CS */\
	mdelay(100);			\
} while(0)

#define __slcd_special_on() 		\
do {	/* RESET# */			\
	__gpio_set_pin(PIN_RESET_N);	\
	mdelay(10);			\
	__gpio_clear_pin(PIN_RESET_N);	\
	mdelay(10);			\
	__gpio_set_pin(PIN_RESET_N);	\
	mdelay(100);			\
	Mcupanel_RegSet(0x0015,0x0050);	\
	Mcupanel_RegSet(0x0011,0x0000);	\
	Mcupanel_RegSet(0x0010,0x3628);	\
	Mcupanel_RegSet(0x0012,0x0002);	\
	Mcupanel_RegSet(0x0013,0x0E47);	\
	udelay(100);			\
	Mcupanel_RegSet(0x0012,0x0012);	\
	udelay(100);			\
	Mcupanel_RegSet(0x0010,0x3620);	\
	Mcupanel_RegSet(0x0013,0x2E47);	\
	udelay(50);			\
	Mcupanel_RegSet(0x0030,0x0000);	\
	Mcupanel_RegSet(0x0031,0x0502);	\
	Mcupanel_RegSet(0x0032,0x0307);	\
	Mcupanel_RegSet(0x0033,0x0304);	\
	Mcupanel_RegSet(0x0034,0x0004);	\
	Mcupanel_RegSet(0x0035,0x0401);	\
	Mcupanel_RegSet(0x0036,0x0707);	\
	Mcupanel_RegSet(0x0037,0x0303);	\
	Mcupanel_RegSet(0x0038,0x1E02);	\
	Mcupanel_RegSet(0x0039,0x1E02);	\
	Mcupanel_RegSet(0x0001,0x0000);	\
	Mcupanel_RegSet(0x0002,0x0300);	\
	if (jzfb.bpp == 16)		\
		Mcupanel_RegSet(0x0003,0x10B8); /*8-bit system interface two transfers*/\
	else				\
		if (jzfb.bpp == 32)	\
			Mcupanel_RegSet(0x0003,0xD0B8);/*8-bit system interface three transfers,666*/\
	Mcupanel_RegSet(0x0008,0x0204);\
	Mcupanel_RegSet(0x000A,0x0008);\
	Mcupanel_RegSet(0x0060,0x3100);\
	Mcupanel_RegSet(0x0061,0x0001);\
	Mcupanel_RegSet(0x0090,0x0052);\
	Mcupanel_RegSet(0x0092,0x000F);\
	Mcupanel_RegSet(0x0093,0x0001);\
	Mcupanel_RegSet(0x009A,0x0008);\
	Mcupanel_RegSet(0x00A3,0x0010);\
	Mcupanel_RegSet(0x0050,0x0000);\
	Mcupanel_RegSet(0x0051,0x00EF);\
	Mcupanel_RegSet(0x0052,0x0000);\
	Mcupanel_RegSet(0x0053,0x018F);\
	/*===Display_On_Function=== */ \
	Mcupanel_RegSet(0x0007,0x0001);\
	Mcupanel_RegSet(0x0007,0x0021);\
	Mcupanel_RegSet(0x0007,0x0023);\
	Mcupanel_RegSet(0x0007,0x0033);\
	Mcupanel_RegSet(0x0007,0x0133);\
	Mcupanel_Command(0x0022);/*Write Data to GRAM	*/  \
	udelay(1);		\
	Mcupanel_SetAddr(0,0);	\
	mdelay(100);		\
} while (0)

#define __slcd_special_off() 		\
do { \
} while(0)
#endif

#ifndef __slcd_special_pin_init
#define __slcd_special_pin_init()
#endif
#ifndef __slcd_special_on
#define __slcd_special_on()
#endif
#ifndef __slcd_special_off
#define __slcd_special_off()
#endif

/*
 * Platform specific definition
 */
#if defined(CONFIG_SOC_JZ4740)
#if defined(CONFIG_JZ4740_PAVO)

/* LCD_D0~LCD_D7, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_8bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x001800ff;	\
	REG_GPIO_PXSELC(2) = 0x001800ff;	\
} while (0)

/* LCD_D0~LCD_D7, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_9bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x001801ff;	\
	REG_GPIO_PXSELC(2) = 0x001801ff;	\
} while (0)

/* LCD_D0~LCD_D15, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_16bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x0018ffff;	\
	REG_GPIO_PXSELC(2) = 0x0018ffff;	\
} while (0)

/* LCD_D0~LCD_D17, SLCD_RS, SLCD_CS */
#define __gpio_as_slcd_18bit()			\
do {						\
	REG_GPIO_PXFUNS(2) = 0x001bffff;	\
	REG_GPIO_PXSELC(2) = 0x001bffff;	\
} while (0)

#define __slcd_dma_enable() (REG_SLCD_CTRL |= SLCD_CTRL_DMA_EN)

#define __slcd_dma_disable() \
do {\
	while (REG_SLCD_STATE & SLCD_STATE_BUSY); 	\
	REG_SLCD_CTRL &= ~SLCD_CTRL_DMA_EN;		\
} while(0)

#define GPIO_PWM    123		/* GP_D27 */
#define PWM_CHN 4    /* pwm channel */
#define PWM_FULL 101
/* 100 level: 0,1,...,100 */
#define __slcd_set_backlight_level(n)\
do { \
__gpio_as_output(32*3+27); \
__gpio_set_pin(32*3+27); \
} while (0)

#define __slcd_close_backlight() \
do { \
__gpio_as_output(GPIO_PWM); \
__gpio_clear_pin(GPIO_PWM); \
} while (0)

#else

#define __slcd_set_backlight_level(n)
#define __slcd_close_backlight()

#endif /* #if defined(CONFIG_MIPS_JZ4740_PAVO) */

#define __slcd_display_pin_init() \
do { \
	__slcd_special_pin_init(); \
} while (0)

#define __slcd_display_on() \
do { \
	__slcd_special_on(); \
	__slcd_set_backlight_level(80); \
} while (0)

#define __slcd_display_off() \
do { \
	__slcd_special_off(); \
	__slcd_close_backlight(); \
} while (0)

#endif /* CONFIG_SOC_JZ4740 */
#endif  /*__JZSLCD_H__*/


