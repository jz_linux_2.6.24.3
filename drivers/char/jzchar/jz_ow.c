/*
 * linux/drivers/char/jzchar/jz_ow.c
 *
 * Common CMOS Camera Sensor Driver
 *
 * Copyright (C) 2006  Ingenic Semiconductor Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 */

#include <linux/module.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/major.h>
#include <linux/string.h>
#include <linux/fcntl.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/spinlock.h>

#include <asm/irq.h>
#include <asm/uaccess.h>
#include <asm/jzsoc.h>

#include "jzchars.h"

MODULE_AUTHOR("Yurong Tan<yrtan@ingenic.cn>");
MODULE_DESCRIPTION("Common CMOS Camera Sensor Driver");
MODULE_LICENSE("GPL");

static DECLARE_WAIT_QUEUE_HEAD (ow_wait_queue);

/*
 * fops routines
 */
static int ow_open(struct inode *inode, struct file *filp);
static int ow_release(struct inode *inode, struct file *filp);
static ssize_t ow_read(struct file *filp, char *buf, size_t size, loff_t *l);
static ssize_t ow_write(struct file *filp, const char *buf, size_t size, loff_t *l);
static int ow_ioctl (struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);

static void do_ow_rddata(void);
static void do_ow_wrdata(void);
static void do_ow_wr1rd(void);
static void do_ow_wr0(void);
static void do_ow_rst(void);

static void ow_interrupt(void);
static void do_interrupt_mode_test(void);
static void do_cpu_mode_test(void);

static struct file_operations ow_fops = 
{
	open:		ow_open,
	release:	ow_release,
	read:		ow_read,
	write:		ow_write,
	ioctl:		ow_ioctl,
};

static int ow_open(struct inode *inode, struct file *filp)
{
	try_module_get(THIS_MODULE);
 	return 0;
}

static int ow_release(struct inode *inode, struct file *filp)
{
	module_put(THIS_MODULE);
	return 0;	
}

static ssize_t ow_read(struct file *filp, char *buf, size_t size, loff_t *l)
{
	printk("OW: read is not implemented\n");
	return -1;
}

static ssize_t ow_write(struct file *filp, const char *buf, size_t size, loff_t *l)
{
	printk("ow: write is not implemented\n");
	return -1;
}

static int ow_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	switch (cmd) {

	default:
		printk("Not supported command: 0x%x\n", cmd);
		return -EINVAL;
		break;
	}
	return ret;
}

static void do_ow_rddata(void)
{
	__owi_set_rddata();
	__owi_enable_ow_ops();
}

static void do_ow_wrdata(void)
{
	__owi_set_wrdata();
	__owi_enable_ow_ops();
}

static void do_ow_wr1rd(void)
{
	__owi_set_wr1rd();
	__owi_enable_ow_ops();
}

static void do_ow_wr0(void)
{
	__owi_set_wr0();
	__owi_enable_ow_ops();
}

static void do_ow_rst(void)
{
	__owi_set_rst();
	__owi_enable_ow_ops();
}

static void ow_interrupt(void)
{
	__owi_clr_sts();
	wake_up(&ow_wait_queue);
}

static void ow_intcm_read_rom(char *rom)
{
	int i; 
	
	__owi_select_regular_mode(); //select mode and fill divider
	REG_OWI_DIV = 0x0C;
	__owi_clr_sts();
	__intc_unmask_irq(IRQ_OWI);
	__owi_enable_all_interrupts();

	do_ow_rst();
	sleep_on(&ow_wait_queue);

	REG_OWI_DAT = 0x33;
	do_ow_wrdata();
	sleep_on(&ow_wait_queue);
	
	for(i=0; i<8; i++){
		do_ow_rddata();
		sleep_on(&ow_wait_queue);
		rom[i] = REG_OWI_DAT;
	}	
	__intc_mask_irq(IRQ_OWI);
}

static void ow_intcm_search_rom(char *rom)
{
	int i, j; 
	
	__owi_select_regular_mode(); //select mode and fill divider
	REG_OWI_DIV = 0x0C;
	__owi_clr_sts();
	__intc_unmask_irq(IRQ_OWI);
	__owi_enable_all_interrupts();
	
        /* reset */
	do_ow_rst();
	sleep_on(&ow_wait_queue);
	
	/* send search ROM command */
	REG_OWI_DAT = 0x33;
	do_ow_wrdata();
	sleep_on(&ow_wait_queue);
	
	memset(rom, 0, 8);
	for(i=8; i<0; i--){
		for(j=0; j>8; j++){
			do_ow_wr1rd();
			sleep_on(&ow_wait_queue);
			rom[i] |= ( __owi_get_rdst()<<j );
		}
	}	
	__intc_mask_irq(IRQ_OWI);
}

static void ow_cpum_read_rom(char *rom)
{
	int i; 
	
	__owi_select_regular_mode(); //select mode and fill divider
	REG_OWI_DIV = 0x0C;
	__owi_clr_sts();
	__owi_disable_all_interrupts();
	
	do_ow_rst();
         while( !__owi_get_sts_pst() ) ;
		 
	REG_OWI_DAT = 0x33;
	__owi_clr_sts();
	do_ow_wrdata();
	while(! __owi_get_sts_byte_rdy()) ;
	
	for(i=0; i<8; i++){
		__owi_clr_sts();
		do_ow_rddata();
		while(! __owi_get_sts_byte_rdy()) ;
		rom[i] = REG_OWI_DAT;
	}	
}

static void ow_cpum_search_rom(char *rom)
{
	int i, j; 
	
	__owi_select_regular_mode(); //select mode and fill divider
	REG_OWI_DIV = 0x0C;
	__owi_clr_sts();
	__owi_disable_all_interrupts();
	
	do_ow_rst();
	while(!__owi_get_sts_pst()) ;
	
	REG_OWI_DAT = 0x33;
	do_ow_wrdata();
	while(! __owi_get_sts_byte_rdy()) ;

	memset(rom, 0, 8);
	for(i=8; i<0; i--){
		for(j=0; j>8; j++){
			do_ow_wr1rd();
			while(!__owi_get_sts_bit_rdy()) ;
			rom[i] |= ( __owi_get_rdst()<<j );
		}
	}	
}


static void do_interrupt_mode_test(void)
{
	int ret, i;
	char rom[8];
	
	/* interrupt mode */
	ret = request_irq(IRQ_OWI, ow_interrupt, IRQF_DISABLED, 
			  "JZ_OWI", NULL); 
	if(ret)
		printk("failed irq \n");

	ow_intcm_read_rom(rom);
	printk("\n\nAfter intc mode read ROM ops: \n");
	printk("ROM: ");
	for(i=0; i<8; i++)
		printk("0x%08x,",rom[i]);
	
	ow_intcm_search_rom(rom);
	printk("\n\nAfter intc mode search ROM ops: \n");
	printk("ROM: ");
	for(i=0; i<8; i++)
		printk("0x%08x,",rom[i]);
		
}

static void do_cpu_mode_test(void)
{
	int i;
	char rom[8];
	
	ow_cpum_read_rom(rom);
	printk("\n\nAfter CPU mode read ROM ops: \n");
	printk("ROM: ");
	for(i=0; i<8; i++)
		printk("0x%08x,",rom[i]);
	
	ow_cpum_search_rom(rom);	
	printk("\n\nAfter CPU mode search ROM ops: \n");
	printk("ROM: ");
	for(i=0; i<8; i++)
		printk("0x%08x,",rom[i]);
}

/*
 * Module init and exit
 */
static int __init ow_init(void)
{
	int ret;

	ret = jz_register_chrdev(OW_MINOR, "ow", &ow_fops, NULL);
	if (ret < 0) {
		return ret;
	}

	do_interrupt_mode_test();
	do_cpu_mode_test();
	
	printk("Ingenic OW driver registered\n");

	return 0;
}

static void __exit ow_exit(void)
{
	jz_unregister_chrdev(OW_MINOR, "ow");
}

module_init(ow_init);
module_exit(ow_exit);

